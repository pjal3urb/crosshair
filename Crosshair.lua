HUDCrosshairBase = HUDCrosshairBase or class()

local gage_path = "units/pd2_dlc1/weapons/wpn_effects_textures/"
local butcher_path = "units/pd2_dlc_butcher_mods/weapons/wpn_effects_textures/"

HUDCrosshairBase._ICONS = {
	--Gage / butcher standard sight reticles
	dot = { texture_switch = true, texture = gage_path .. "wpn_sight_reticle_s_1" },
	cross1 = { texture_switch = true, texture = gage_path .. "wpn_sight_reticle_2" },
	cross2 = { texture_switch = true, texture = gage_path .. "wpn_sight_reticle_3" },
	cross3 = { texture_switch = true, texture = gage_path .. "wpn_sight_reticle_4" },
	circle1 = { texture_switch = true, texture = gage_path .. "wpn_sight_reticle_5" },
	circle2 = { texture_switch = true, texture = gage_path .. "wpn_sight_reticle_6" },
	circle3 = { texture_switch = true, texture = gage_path .. "wpn_sight_reticle_7" },
	circle4 = { texture_switch = true, texture = gage_path .. "wpn_sight_reticle_8" },
	angle1 = { texture_switch = true, texture = gage_path .. "wpn_sight_reticle_9" },
	angle2 = { texture_switch = true, texture = gage_path .. "wpn_sight_reticle_10"},
	first_circle = { texture_switch = true, texture = butcher_path .. "wpn_sight_reticle_11"},
	flat = { texture_switch = true, texture = butcher_path .. "wpn_sight_reticle_12"},
	sun = { texture_switch = true, texture = butcher_path .. "wpn_sight_reticle_13"},
	hunter = { texture_switch = true, texture = butcher_path .. "wpn_sight_reticle_14"},
	on_off = { texture_switch = true, texture = butcher_path .. "wpn_sight_reticle_15"},
	cross = { texture_switch = true, texture = butcher_path .. "wpn_sight_reticle_16"},
	insert_here = { texture_switch = true, texture = butcher_path .. "wpn_sight_reticle_17"},
	hashtag = { texture_switch = true, texture = butcher_path .. "wpn_sight_reticle_18"},
	overkill = { texture_switch = true, texture = butcher_path .. "wpn_sight_reticle_19"},
	starbreeze = { texture_switch = true, texture = butcher_path .. "wpn_sight_reticle_20"},
	fuck_you = { texture_switch = true, texture = butcher_path .. "wpn_sight_reticle_21"},
	rock_on = { texture_switch = true, texture = butcher_path .. "wpn_sight_reticle_22"},
	lion_game_lion = { texture_switch = true, texture = butcher_path .. "wpn_sight_reticle_23"},
	--Standard hit confirm
	hit_confirm = { scale = 0.5, texture = "guis/textures/pd2/hitconfirm" },
	crit_confirm = { scale = 0.5, texture = "guis/textures/pd2/hitconfirm_crit" }
}

HUDCrosshairBase._TEXTURE_SWITCHES = {
	[Color.red] = "_il",
	[Color.yellow] = "_yellow_il",
	[Color.green] = "_green_il",
	[Color.blue] = "_blue_il"
}

function HUDCrosshairBase:init(parent, name)
	self._name = name
	self._parent_panel = parent
	self._panel = self._parent_panel:panel({
		name = name,
		h = self._parent_panel:h(),
		w = self._parent_panel:w(),
		visible = true,
		layer = 0,
	})
	self._panel:set_center(self._parent_panel:w() / 2, self._parent_panel:h() / 2)
	
	self:_clear()
end

function HUDCrosshairBase:setup(data)
	self:_clear()
	
	for name, crosshair_data in pairs(data) do
		if not self:_add(name, crosshair_data) then
			self:_clear()
			return false
		end
	end
	
	return true
end

function HUDCrosshairBase:delete()
	self:_clear()
	self._parent_panel:remove(self._panel)
end

function HUDCrosshairBase:_clear()
	for name, _ in pairs(self._crosshairs or {}) do
		self:_remove(name)
	end
	
	self._crosshairs = {}
	self._crosshair_count = 0
	self._active_crosshair = nil
end

function HUDCrosshairBase:_remove(name)
	local sub_panel = self._crosshairs[name] and self._crosshairs[name].panel
	if sub_panel then
		for _, child in ipairs(sub_panel:children()) do
			sub_panel:remove(child)
		end
		
		self._panel:remove(sub_panel)
		self._crosshair_count = self._crosshair_count - 1
		self._crosshairs[name] = nil
	end
end

function HUDCrosshairBase:_add(name, data)
	local sub_panel = self._panel:panel({
		name = name,
		h = self._panel:h(),
		w = self._panel:w(),
		layer = 0,
		visible = false,
	})
	sub_panel:set_center(self._panel:w() / 2, self._panel:h() / 2)
	
	local new_crosshair = {
		name = name,
		panel = sub_panel,
		components = {},
	}
	
	for i, component in ipairs(data) do
		local icon = HUDCrosshairBase._ICONS[component.icon]
		
		if icon then
			local texture = icon.texture .. (icon.texture_switch and (HUDCrosshairBase._TEXTURE_SWITCHES[component.color] or HUDCrosshairBase._TEXTURE_SWITCHES[Color.red]) or "")
			local color = icon.texture_switch and Color.white or component.color or Color.red
			local scale = math.max((component.scale or 1) * (icon.scale or 1), 0)
			local alpha = math.max(component.alpha or 1, 0)
			local duration = math.max(component.duration or 0.25, 0)
			local rotation = component.rotation or 0
			local blend_mode = component.blend_mode or "add"
			local animate_cbk = component.animate_cbk or callback(self, self, "_animate_default")
			
			local j = 0
			while alpha > 0 do
				j = j + 1
				local bitmap = sub_panel:bitmap({
					name = (component.name or (name .. "_" .. tostring(i))) .. "_" .. tostring(j),
					texture = texture,
					valign = "center",
					halign = "center",
					color = color,
					alpha = math.min(alpha, 1),
					rotation = rotation,
					h = math.round(50 * scale),
					w = math.round(50 * scale),
					blend_mode = blend_mode,
					visible = true,
					layer = 0,
				})
				bitmap:set_center(sub_panel:w() / 2, sub_panel:h() / 2)
				table.insert(new_crosshair.components, { bitmap = bitmap, alpha = math.min(alpha, 1), duration = duration, animate_cbk = animate_cbk })
				alpha = alpha - 1
			end
		else
			io.write("ERROR: Invalid icon for component " .. i .. " in " .. self._name .. " / " .. name .. "\n")
			return false
		end
	end
	
	self._active_crosshair = self._active_crosshair or name
	self._crosshairs[name] = new_crosshair
	self._crosshair_count = self._crosshair_count + 1
	
	return true
end

function HUDCrosshairBase:select_crosshair(name)
	if not self._crosshairs[name] then
		return false
	end
	
	if self._active_crosshair ~= name then
		local old = self._crosshairs[self._active_crosshair]
		local new = self._crosshairs[name]
		new.panel:set_visible(old.panel:visible())
		old.panel:set_visible(false)
		self._active_crosshair = name
	end
	
	return true
end

function HUDCrosshairBase:set_visible(status)
	local current_crosshair = self._crosshairs[self._active_crosshair]
	if not current_crosshair then
		return false
	end
	
	current_crosshair.panel:set_visible(status)
	return true
end

function HUDCrosshairBase:visible()
	return self._crosshairs[self._active_crosshair] and self._crosshairs[self._active_crosshair].panel:visible() or false
end

function HUDCrosshairBase:on_hit_confirmed()
	local crosshair = self._crosshairs[self._active_crosshair]
	if crosshair then
		crosshair.panel:set_visible(true)
		for i, data in ipairs(crosshair.components or {}) do
			data.bitmap:stop()
			data.bitmap:animate(data.animate_cbk, data.alpha, data.duration)
		end
	end
end

function HUDCrosshairBase:_animate_default(obj, init_alpha, duration)
	obj:set_visible(true)
	obj:set_alpha(init_alpha)
	local t = duration
	while t > 0 do
		local dt = coroutine.yield()
		t = t - dt
		obj:set_alpha(t/duration * init_alpha)
	end
	obj:set_visible(false)
end

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

HUDCrosshairNew = HUDCrosshairNew or class(HUDCrosshairBase)
function HUDCrosshairNew:init(parent)
	HUDCrosshairNew.super.init(self, parent, "crosshair")
end

HUDHitConfirmNew = HUDHitConfirmNew or class(HUDCrosshairBase)
function HUDHitConfirmNew:init(parent)
	HUDHitConfirmNew.super.init(self, parent, "hit_confirm")
end

HUDCritConfirmNew = HUDCritConfirmNew or class(HUDCrosshairBase)
function HUDCritConfirmNew:init(parent)
	HUDCritConfirmNew.super.init(self, parent, "crit_confirm")
end

HUDHeadshotConfirmNew = HUDHeadshotConfirmNew or class(HUDCrosshairBase)
function HUDHeadshotConfirmNew:init(parent)
	HUDHeadshotConfirmNew.super.init(self, parent, "headshot_confirm")
end

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


local on_hit_confirmed_original = HUDManager.on_hit_confirmed
local on_headshot_confirmed_original = HUDManager.on_headshot_confirmed
local on_crit_confirmed_original = HUDManager.on_crit_confirmed

function HUDManager:on_hit_confirmed(...)
	if managers.user:get_setting( "hit_indicator" ) then
		if self._hud_hit_confirm_new then
			return self._hud_hit_confirm_new:on_hit_confirmed()
		else
			return on_hit_confirmed_original(self, ...)
		end
	end
end

function HUDManager:on_headshot_confirmed(...)
	if managers.user:get_setting( "hit_indicator" ) then
		if self._hud_headshot_confirm_new then
			self._hud_headshot_confirm_new:on_hit_confirmed()
		else
			return on_headshot_confirmed_original(self, ...)
		end
	end
end

function HUDManager:on_crit_confirmed(...)
	if managers.user:get_setting( "hit_indicator" ) then
		if self._hud_crit_confirm_new then
			self._hud_crit_confirm_new:on_hit_confirmed()
		else
			return on_crit_confirmed_original(self, ...)
		end
	end
end

function HUDManager:setup_crosshair(components) --New
	self._hud_crosshair_new = self._hud_crosshair_new or HUDCrosshairNew:new(managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel)
	if not components or not self._hud_crosshair_new:setup(components) then
		self._hud_crosshair_new:delete()
		self._hud_crosshair_new = nil
		return false
	end
	
	return true
end

function HUDManager:setup_hit_confirm(components) --New
	self._hud_hit_confirm_new = self._hud_hit_confirm_new or HUDHitConfirmNew:new(managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel)
	if not components or not self._hud_hit_confirm_new:setup(components) then
		self._hud_hit_confirm_new:delete()
		self._hud_hit_confirm_new = nil
		return false
	end
	
	return true
end

function HUDManager:setup_crit_confirm(components) --New
	self._hud_crit_confirm_new = self._hud_crit_confirm_new or HUDCritConfirmNew:new(managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel)
	if not components or not self._hud_crit_confirm_new:setup(components) then
		self._hud_crit_confirm_new:delete()
		self._hud_crit_confirm_new = nil
		return false
	end
	
	return true
end

function HUDManager:setup_headshot_confirm(components) --New
	self._hud_headshot_confirm_new = self._hud_headshot_confirm_new or HUDHeadshotConfirmNew:new(managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2).panel)
	if not components or not self._hud_headshot_confirm_new:setup(components) then
		self._hud_headshot_confirm_new:delete()
		self._hud_headshot_confirm_new = nil
		return false
	end
	
	return true
end

function HUDManager:crosshair_set_visible(status) --New
	if self._hud_crosshair_new then
		return self._hud_crosshair_new:set_visible(status)
	end
end

function HUDManager:crosshair_visible() --New
	return self._hud_crosshair_new and self._hud_crosshair_new:visible()
end

function HUDManager:crosshair_select(name) --New
	if self._hud_crosshair_new then
		return self._hud_crosshair_new:select_crosshair(name)
	end
end

function HUDManager:hit_confirm_select(name) --New
	if self._hud_hit_confirm_new then
		return self._hud_hit_confirm_new:select_crosshair(name)
	end
end

function HUDManager:crit_confirm_select(name) --New
	if self._hud_crit_confirm_new then
		return self._hud_crit_confirm_new:select_crosshair(name)
	end
end

function HUDManager:headshot_confirm_select(name) --New
	if self._hud_headshot_confirm_new then
		return self._hud_headshot_confirm_new:select_crosshair(name)
	end
end