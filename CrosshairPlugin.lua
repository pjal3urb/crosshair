--[[
	* SETTINGS *
	A crosshair and hit confirm settings table is composed of a series of nested tables, with the following structure:
		<main table> = {
			<crosshair name 1> = {
				{ <component 1> },
				{ <component 2> },
				...
			},
			<crosshair name 2> = {
				{ <component 1> },
				...
			},
			...
		}
	
	The main table can contain any number of crosshairs, and each crosshair can contain any number of components. Components are defined by providing certain attributes, currently:
		scale
			(Optional) Size scale of texture, [0; inf], default 1
		alpha
			(Optional) Visibility of texture. Default 1. Normally restricted to [0; 1], but higher values will add additional layers for more solid colors (use at own risk)
		rotation
			(Optional) Clockwise rotation of texture in degrees, default 0
		icon
			Pre-defined texture of component. Must use one of the following values:
				Standard sights: dot, cross1, cross2, cross3, cricle1, cricle2, cricle3, cricle4, angle1, angle2, 
					first_circle, flat, sun, hunter, on_off, cross, insert_here, hashtag, overkill, starbreeze, fuck_you, rock_on, lion_game_lion
				Standard hit confirm reticles: hit_confirm, crit_confirm
		color
			(Optional) Color of texture, default Color.red
				Standard sights support (Color.) red, blue, green, yellow
				Hit confirmation reticles support any color (e.g. Color.red or Color(1,0,0)), although may look strange for certain values due to transparency channels in the textures
			
	Hit confirmation tables also support the following attributes:
		duration
			(Optional) Duration, in seconds, of the hit confirmation flash. [0; inf], default 0.25
		animate_cbk
			(Optional) Callback function for handling animation on hit confirmation, overriding the default flash. For advanced users only.
				As an example, consider this function:
					function custom_animate(<custom args>, obj, init_alpha, duration) <do something> end
				Setting the component to use this function would then be done by:
					animate_cbk = callback(nil, _G, "custom_animate", <custom ars>)
				On a hit confirm call, the component would then call this function (once per whole alpha value due to stacking of components on >1 alpha attribute). The function will have to handle the animation, and should before it terminates make the component invisible or transparent to prevent it from permanently showing.
				
	All types of hit confirmations and crosshairs are defined in the same way, and can be treated in the same way. If so desired, the same table can be used for all different types. If multiple entries are defined, the first one processed (not necessarily the first one defined) will be used as default crosshair.
	Note however, that while headshot confirm is supported, the vanilla game does not make use of this. If a separate confirmation type for headshots is desired, it must be implemented separate from this crosshair script.
	
	Crosshairs and hit confirmation settings can be reloaded on the fly in-game by calling functions in "managers.hud":
		setup_crosshair(<crosshair table>), setup_hit_confirm(<crosshair table>), setup_crit_confirm(<crosshair table>), setup_headshot_confirm(<crosshair table>)
			Clears old entries and sets up a new layout defined by the input. If nil is sent, the game will revert to using the defaults (no crosshair, cross for hit indicators). If an empty table is sent, crosshair/hit indicator is disabled.
		crosshair_set_visible(<visible>)
			Shows or hides the crosshair.
		crosshair_visible()
			Returns the visibility status of the current crosshair
		crosshair_select(<name>), hit_confirm_select(<name>), crit_confirm_select(<name>), headshot_confirm_select(<name>)
			Switches the crosshair/hit confirm to the specified name, as defined in the settings. If the old crosshair was already visible, the new one will be made visible as well. Will not migrate animation states for hit confirms. Returns true if successfully switched, else false
		
	These functions can be called from anywhere, including your own scripts as long as the crosshair library has been loaded first; e.g.:
		managers.hud:crosshair_select("my_crosshair")
		managers.hud:crosshair_set_visible(true)
]]


local crosshair = {
	no_target = {
		{ scale = 1, alpha = 1.0, icon = "dot", color = Color.blue },
		{ scale = 1.5, alpha = 0.5, icon = "cross3", color = Color.blue },
	},
	hostage = {
		{ scale = 1, alpha = 1.0, icon = "dot", color = Color.green },
		{ scale = 1.5, alpha = 0.5, icon = "cross3", color = Color.green },
	},
	special_enemy = {
		{ scale = 1, alpha = 1.0, icon = "dot", color = Color.red },
		{ scale = 1.5, alpha = 0.5, icon = "cross3", color = Color.red },
		{ scale = 3.0, alpha = 0.25, icon = "angle2", color = Color.red },
	},
	enemy = {
		{ scale = 1, alpha = 1.0, icon = "dot", color = Color.red },
		{ scale = 1.5, alpha = 0.5, icon = "cross3", color = Color.red },
	},
}

local hit_confirm = {
	default = {
		{ scale = 1, alpha = 5.0, icon = "dot", color = Color.red, duration = 0.5 },
		{ scale = 1.5, alpha = 5.0, icon = "cross3", color = Color.red, duration = 0.5 },
	},
}

local crit_confirm = {
	default = {
		{ scale = 1, alpha = 5, icon = "dot", color = Color.yellow, duration = 0.5 },
		{ scale = 1.5, alpha = 5, icon = "cross3", color = Color.yellow, duration = 0.5},
	},
}

local headshot_confirm = {
	default = {
		{ scale = 1, alpha = 5, icon = "dot", color = Color.red, duration = 0.5 },
		{ scale = 1.5, alpha = 5, icon = "cross3", color = Color.red, duration = 0.5},
		{ scale = 3.0, alpha = 5, icon = "angle2", color = Color.red, duration = 0.5 },
	},
}

------------------------------------------------------------------------------------------------------------------------

local init_original = PlayerStandard.init
local update_original = PlayerStandard.update

function PlayerStandard:init(...)
	if managers.hud then
		if managers.hud:setup_crosshair(crosshair or nil) then
			self._crosshair_targets = { "camera", "criminal", "enemy", "civilian", "hostage" }
			self._current_crosshair = "no_target"
			managers.hud:crosshair_select(self._current_crosshair)
			managers.hud:crosshair_set_visible(true)
		end
		managers.hud:setup_hit_confirm(hit_confirm or nil)
		managers.hud:setup_crit_confirm(crit_confirm or nil)
		managers.hud:setup_headshot_confirm(headshot_confirm or nil)
	end
	
	return init_original(self, ...)
end

function PlayerStandard:update(...)
	self:_update_crosshair()
	return update_original(self, ...)
end

function PlayerStandard:_update_crosshair()
	local crosshair_name = "no_target"
	local target

	if self._fwd_ray_new then	--For use with the custom ForwardRay
		local hits = self._fwd_ray_new:get_targets_by_type(self._crosshair_targets, true, true)
		target = hits[1] and hits[1].unit
	elseif self._fwd_ray then	--Vanilla compatibility
		target = alive(self._fwd_ray.unit) and self._fwd_ray.unit
		if target:in_slot(1) and not (target:base() and target:base().security_camera) then
			target = nil
		end
	end
	
	if target then
		if self._last_unit ~= target then
			if target:in_slot(12) then	--Enemy
				if managers.groupai:state():is_enemy_special(target) or (target:base()._tweak_table == "sniper") then
					crosshair_name = "special_enemy"
				else
					crosshair_name = "enemy"
				end
			elseif target:in_slot(21) or target:in_slot(22) then	--Civilian/hostage
				crosshair_name = "hostage"
			elseif target:in_slot(1) then	--Camera
				crosshair_name = "enemy"
			end
		else
			crosshair_name = self._current_crosshair
		end
	end
	
	self._last_unit = target
	
	if self._current_crosshair ~= crosshair_name then
		self._current_crosshair = crosshair_name
		managers.hud:crosshair_select(crosshair_name)
	end
end
